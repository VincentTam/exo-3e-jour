% Exos du 3e jour
% <a href="https://git.io/vtam">Vincent Tam</a>
% 31 mars 2023

# Méthode récursive : syntaxe primitive

Les deux syntaxes ont le même sens sémantique.

`x = 0` ici est une **égalité** plutôt qu'une assignation.

```
fonction fact(x)
début
  si (x < 0) alors
    erreur mismatch
    sort de la fonction
  fin
  si (x = 0)
    alors retourner 1
  sinon
    retourner x * fact(x - 1)
fin
```

Question : pourquoi c'est récursive ?  
Réponse : dans le corps de la fonction (e.g. `fact()`), celle-ci est invoquée.

# Méthode récursive : syntaxe condensée

Avec l'opérateur ternaire

    cond : expression si cond est vrai : expression si faux

Le sens est le même, mais la syntaxe ci-dessous est moins longue.

```
fonction fact(x)
début
  si (x < 0) alors
    erreur mismatch
    sort de la fonction
  fin
  y : entier
  y = (x = 0) ? 1 : x * fact(x - 1)
  retourner y
fin
```

On entourne `x = 0` par des `()` pour ne pas le confondre avec l'assignation `=`
à gauche.

# Méthode itérative

On utilise une boucle pour des tâches répétitives.

```
fonction fact(x)
début
  si x < 0 alors
    erreur mismatch
    sort de la fonction
  fin
  résultat ← 1
  pour i allant de 1 à x par pas de 1 faire
    résultat ← résultat + i
  finpour
  retourner résultat
fin
```

Observations :

1. Dans la boucle `tant que (cond) … fin`, la `(cond)` est vérifiée.
1. Sorti de la boucle, la négation `(¬cond)` est vérifiée.
1. Il est possible de ne pas passer par le conditionnel `si (x == 0) ... finsi`.
